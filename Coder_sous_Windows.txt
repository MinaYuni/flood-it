Pour ceux qui n'ont pas de linux installé sur un ordinateur.

Trois idées possibles:

------------------------------------------------------
** une solution simple 

    1) Installation d'un terminal linux :

       https://linuxconfig.org/how-to-install-ubuntu-18-04-on-windows-10

    2) Lancer ubuntu

    3) Ouvrir un terminal et entrer les commandes suivantes :

                        sudo apt update
                        sudo apt install make
                        sudo apt install gcc

      puis installer SDL1.2  ou   SDL2
                        sudo apt install libdev1.2-dev
			ou
			sudo apt install libsdl2-dev



     4) Pour accéder à vos fichiers sauvegardés sous Windows :

                     cd /mnt/c

     Dans cette solution, le rendu des images du projet Flood-It n'est pas aussi beau que la capture d'écran du pdf mais on peut tout de même voir l'effet d'inondation

----------------------------------------------------------

** utiliser le logiciel minGW en installant la bibliothèque SDL
Pour cela voir
    https://w3.cs.jmu.edu/bernstdh/Web/common/help/cpp_mingw-sdl-setup.php
ou cette vidéo en français :
    https://www.youtube.com/watch?v=UQEb6kkjXrE



Aide installation SDL  proposée par Dan Serraf:


1 installe mingw sous  windows , lien Youtube : https://youtu.be/kG1c8ZOb6rM

2 installe makefile , lien Youtube : https://youtu.be/-riHEHGP2DU 
(Pour ce qui l'ont pas sous windows , les premieres minutes suffisent pour l'installation )

3 installe SDL2 , lien Youtube : https://youtu.be/Lwx9rSgwoDg

4 charge le projet sous le site de l'UE : telecharger le projet SDL2 , lien html : https://www-licence.ufr-info-p6.jussieu.fr/lmd/licence/2019/ue/LU2IN006-2020fev/

5 copier les repertoires lib,bin,include du dossier SDL installe precdemment et les collés dans le dossier du projet .

6 Modifier l'entete dans le fichier API_grille.h , remplace </SDL/SDH.h> par "./include/SDL.h"
(Chez moi y me semble qui manque un include stdio ou stdlib lorsque j ai telecharge le dossier a verifier chez vous ... )

7 Modifier Makefile les lignes suivantes :
  Flood-It_Partie1: Flood-It_Partie1.o Liste_case.o  API_Grille.o API_Gene_instance.o Fonctions_exo1.o 
	gcc -o bin/Flood-It_Partie1 Flood-It_Partie1.o Liste_case.o API_Grille.o API_Gene_instance.o Fonctions_exo1.o  -I include -L lib -lmingw32 -lSDL2main -lSDL2

- Compilation à partir de Makefile.bin donné ci-dessous


Commande : cd ...\Partie_1_en_ligne_SDL2\ 
Commande : make
Commande : .\bin\Flood-It_Partie1.exe 10 7 5 5 0 1 (par exemple)


- Contenu du fichier: Makefile.bin

all: Flood-It_Partie1

API_Gene_instance.o: API_Gene_instance.c API_Gene_instance.h
	gcc -c API_Gene_instance.c

API_Grille.o: API_Grille.c API_Grille.h
	gcc -c API_Grille.c

Liste_case.o: Liste_case.c Liste_case.h
	gcc -c Liste_case.c

Fonctions_exo1.o: Fonctions_exo1.c Entete_Fonctions.h Liste_case.h
	gcc -c Fonctions_exo1.c

Flood-It_Partie1.o: Flood-It_Partie1.c
	gcc -c Flood-It_Partie1.c 

Flood-It_Partie1: Flood-It_Partie1.o Liste_case.o  API_Grille.o API_Gene_instance.o Fonctions_exo1.o 
	gcc -o bin/Flood-It_Partie1 Flood-It_Partie1.o Liste_case.o API_Grille.o API_Gene_instance.o Fonctions_exo1.o  -I include -L lib -lmingw32 -lSDL2main -lSDL2

clean:
	rm -f *.o Flood-It_Partie1




--------------------------------------------------------------

** utiliser une distribution linux sur clef USB bootable
  (en pensant à sauvegarder les fichiers avant de refmer)
  c'est assez simple à réaliser en suivant des tutos pour ubuntu par exemple



