#include <stdio.h>
#include <stdlib.h> 
#include <time.h>

#include "graphe_zone.h"
#include "API_Grille.h"
#include "API_Gene_instance.h"

int main (int argc, char**argv) {
	int dim, nbcl, nivdif, graine, exo, aff;
	int i,j;
	int **M;
	
	clock_t
		temps_initial, /* Temps initial en micro-secondes */
		temps_final;   /* Temps final en micro-secondes */
	float
		temps_cpu;     /* Temps total en secondes */

	if (argc != 7) {
		printf("usage : %s <dimension> <nb_de_couleurs> <niveau_difficulte> <graine> <exo 5/6> <aff 0/1>\n", argv[0]);
		return 1;
	}

	dim = atoi(argv[1]);
	nbcl = atoi(argv[2]);
	nivdif = atoi(argv[3]);
	graine = atoi(argv[4]);
	exo = atoi(argv[5]);
	aff = atoi(argv[6]);

	/* Allocation puis Generation de l'instance */
	M = (int **)malloc(sizeof(int*)*dim);
	for (i=0 ; i<dim ; i++) {
		M[i] = (int*)malloc(sizeof(int)*dim);
		if (M[i] == 0) printf("Pas assez d'espace memoire disponibles \n");
	}
	Gene_instance_genere_matrice(dim, nbcl, nivdif, graine, M);

	/* Allocation du graphe */
	Graphe_zone *graphe = (Graphe_zone*)malloc(sizeof(Graphe_zone)); 
	if (graphe == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
	/* Creation du graphe entier */
	cree_graphe_zone(M, graphe, dim, nbcl);
	
	/* Affichage du graphe */
	if (aff == 1) { 
		affiche_graphe(graphe,dim);
	}

	/* Allocation de la zsg */
	Cellule_som *liste_sommet_zsg = (Cellule_som*)malloc(sizeof(Cellule_som));
	if (liste_sommet_zsg == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 1; 
	}
	
	/* Initialisation de la zsg et de sa bordure */
	liste_sommet_zsg->sommet = graphe->mat[0][0];
	liste_sommet_zsg->suiv = NULL;
	graphe->mat[0][0]->marque = 0;
	graphe->mat[0][0]->distance=0;
	graphe->mat[0][0]->pere=NULL;
	bordure_graphe(graphe, &liste_sommet_zsg, M[0][0]);
	
	
	/* -- Initialisation du jeu terminee -- */
	
	
	/* Affichage du graphe apres initialisation */
	if (aff == 1) {
		affiche_graphe(graphe,dim);
	}
	
	/* Affichage du nombre de coups pour terminer le jeu et son temps de resolution */
	temps_initial = clock ();
	
	/* le depart du jeu est la zsg donc en (0,0) et la cible est le coin inferieur droit donc en (dim-1,dim-1) */
	// strategie max-bordure
	if (exo == 5) {
        int success = -1; 
        int *resultat;
        printf("Exo 5, nombre de coups : %d ", jeu(graphe, &liste_sommet_zsg, nbcl, resultat, success));
    }
    // parcours en largeur 
    else if(exo == 6) {
        int success = BFS(graphe, graphe->mat[0][0], graphe->mat[dim-1][dim-1]);
        int *resultat = chemin_inverse(graphe->mat[dim-1][dim-1]);
        printf("Exo 6, nombre de coups : %d ", jeu(graphe, &liste_sommet_zsg, nbcl, resultat, success));
    }

	temps_final = clock ();
	temps_cpu = (temps_final - temps_initial) * 1e-6;
	
	printf("en %f s \n",temps_cpu);


	/* Desallocation de la zsg et du graphe */
	detruit_liste_sommet(liste_sommet_zsg);
	detruit_liste_sommet(graphe->som);

	/* Desallocation de la matrice */
	for(i=0 ; i<dim ; i++) {
		if (M[i])
			free(M[i]);
	} 
	if (M) free(M);

	return 0;
}
