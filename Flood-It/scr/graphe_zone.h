#ifndef __GRAPHE_ZONE__
#define __GRAPHE_ZONE__

#include "Liste_case.h"
#include "API_Grille.h"
#include "API_Gene_instance.h"

typedef struct sommet Sommet ;

/* Element d’une liste chainee de pointeurs sur Sommets */
typedef struct cellule_som {
	Sommet *sommet;
	struct cellule_som *suiv;
} Cellule_som;


struct sommet {
	int num; 			/* Numero du sommet (sert uniquement a l’affichage) */
	int cl; 			/* Couleur d’origine du sommet-zone */
	ListeCase cases; 	/* Listes des cases du sommet-zone */
	int nbcase_som; 	/* Nombre de cases de cette liste */
	int marque;			/**/
	int distance;		/**/
	Sommet* pere;		/**/ 
	Cellule_som *sommet_adj; /* Liste des aretes pointeurs sur les sommets adjacents */
};

typedef struct graphe_zone {
	Sommet *** mat; 	/* Matrice de pointeurs sur les sommets indiquant a quel sommet appartient une case (i,j) de la grille */
	int nbsom; 			/* Nombre de sommets dans le graphe */
	Cellule_som *som; 	/* Liste chainee des sommets du graphe */
		
} Graphe_zone;


/* 
 * Ajoute un pointeur sur Sommet a une liste chainee de Cellule_som passee en parametre 
 *
 * Entrees : 
 * cell_som : liste chainee de sommets
 * s : sommet a ajouter a la liste 
 *
 * Sortie : 
 * Retourne 1 si le sommet a bien ete ajoute, 0 sinon
 */
int ajoute_liste_sommet(Cellule_som **cell_som, Sommet *s);


/* 
 * Detruit une liste chainee de Cellule som SANS detruire les sommets pointees par cette liste 
 *
 * Entrees : 
 * cell_som : liste chainee de sommets a detruire 
 *
 * Sortie : 
 * void
 */
void detruit_liste_sommet(Cellule_som *cell_som);


/* 
 * Met a jour deux sommet s1 et s2 en indiquant qu’ils sont adjacents l’un de l’autre 
 *
 * Entrees : 
 * s1, s2 : les sommets a ajouter en tant que sommets adjacents
 *
 * Sortie : 
 * Retoune 1 si l'ajout a bien ete fait, 0 sinon 
 */ 
int ajoute_voisin(Sommet *s1, Sommet *s2);


/* 
 * Teste si les sommets s1 et s2 sont adjacents 
 *
 * Entrees : 
 * s1, s2 : les sommets a tester s'ils sont adjacents 
 *
 * Sortie : 
 * Retourne 1 si et seulement si deux sommets sont adjacents, 0 sinon
 */
int adjacent(Sommet *s1, Sommet *s2);

/* 
 * Determine la zone correspondante a la case (x,y) de la grille quand graphe.mat[x][y]==NULL 
 *
 * Entrees : 
 * M : matrice entiere carree de dimension dim*dim DEJA ALLOUEE 
 * dim : dimension de la matrice 
 * graphe : structure de type Graphe_zone 
 * x, y : coordonées de la case 
 *
 * Sortie : 
 * Retourne 1 si la zone a ete trouvee, 0 sinon
 */
int trouve_zone(int **M, int dim, Graphe_zone *graphe, int x, int y);


/* 
 * Cree le graphe entier 
 *
 * Entrees : 
 * M : matrice entiere carree de dimension dim*dim DEJA ALLOUEE 
 * graphe : graphe qui va etre cree DEJA ALLOUEE
 * dim : dimension de la matrice
 * nbcl : nombre de couleurs du jeu 
 *
 * Sortie : 
 * Retourne 1 si le graphe a bien ete cree, 0 sinon
 */
int cree_graphe_zone(int **M, Graphe_zone *graphe, int dim, int nbcl);


/* 
 * Affiche le graphe par numero de sommet
 *
 * Entrees : 
 * graphe : graphe de dimension dim*dim
 * dim : dimension du graphe 
 *
 * Sortie : 
 * void
 */
void affiche_graphe(Graphe_zone *graphe, int dim); 


/* 
 * Affiche une liste chainee Cellule_som 
 *
 * Entrees : 
 * cell_som : liste chaine a afficher 
 *
 * Sortie : 
 * Retourne 0 si la liste est vide, 1 sinon
 */
int affiche_cellule_som(Cellule_som *cell_som); 


/*
 * Met a jour le graphe dont la zsg et sa bordure
 *
 * Entrees : 
 * graphe : graphe du jeu 
 * zsg : liste des sommets de la zsg 
 * cl : couleur selon laquelle le graphe se met a jour
 *
 * Sortie : 
 * Retourne 1 si la mise a jour a bien ete faite, 0 sinon
 */
int bordure_graphe(Graphe_zone *graphe, Cellule_som **zsg, int cl);
 

/* 
 * Regarde si le jeu est terminee 
 * Le jeu s’arrête quand la bordure est vide pour toute couleur 
 *
 * Entrees : 
 * graphe : graphe du jeu 
 *
 * Sortie : 
 * Retourne 1 si le jeu doit s'arreter, 0 sinon
 */
int check_fin(Graphe_zone *graphe);


/* 
 * Cherche l'indice de la valeur maximale d'un tableau 
 *
 * Entrees : 
 * tab : un tableau d'entier 
 * taille : taille du tableau 
 *
 * Sortie : 
 * Retourne l'indice de la valeur maximale du tableau
 */
int max_tableau(int *tab, int taille);


/* 
 * Cherche la couleur de la bordure qui a le plus de sommets 
 *
 * Entrees : 
 * graphe : graphe du jeu 
 * nbcl : nombre de couleurs du graphe 
 *
 * Sortie : 
 * Retourne la couleur qui a le plus de sommets 
 */
int max_bordure(Graphe_zone *graphe, int nbcl);


/* 
 * Resolve du jeu 
 *
 * Entrees : 
 * graphe : graphe du jeu 
 * zsg : liste des sommets de la zsg 
 * nbcl : nombre de couleurs du graphe 
 * BFS : 
 * taille_bfs : 
 *
 * Sortie : 
 * Retourne le nombre de coups pour terminer le jeu
 */
int jeu(Graphe_zone *graphe, Cellule_som **zsg,int nbcl, int *BFS, int taille_bfs);


/* 
 * Parcours en largeur du graphe jusqu'au sommet cible 
 *
 * Entrees : 
 * graphe : graphe du jeu 
 * depart : sommet de depart
 * cible : sommet cible 
 *
 * Sortie : 
 * Retourne la distance a laquelle on a trouve le sommet cible 
 */
int BFS(Graphe_zone *graphe, Sommet *depart, Sommet *cible);


/* 
 * Cherche le chermin inverse a parcourir a partir du sommet vise 
 * jusqu'au sommet de base qui est le sommet qui appartient a la zsg au debut du jeu 
 *
 * Entrees : 
 * sommet : sommet vise 
 *
 * Sortie : 
 * Retourne le chemin inverse 
 */
int *chemin_inverse(Sommet *vise); 



#endif
