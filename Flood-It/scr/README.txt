-----------------------------------------------------------
Sorbonne Université - L2 mono Informatique (2019-2020) 
LU2IN006 - Structure de Données 
Projet Flood-It
Doan LE (3875020) - Groupe 2 
-----------------------------------------------------------

Compiler avec le Makefile avec la commande make

*****

Partie 1 : 
Pour jouer, veuillez exécuter 
Flood-It_Partie1 <dimension> <nb_de_couleurs> <niveau_difficulte> <graine> <exo 0/2> <aff 0/1>
avec exo : 
0 pour l'exercice 1 (aléatoire récursif) 
2 pour l'exercice 3 (structure acyclique) 
avec aff : 
pour l'affichage ou non de la grille avec les couleurs 

*****

Partie 2 : 
Pour jouer, veuillez exécuter 
Flood-It_Partie2 <dimension> <nb_de_couleurs> <niveau_difficulte> <graine> <exo 5/6> <aff 0/1> 
avec exo : 
5 pour l'exercice 5 (stratégie max-bordure) 
6 pour l'exercice 6 (parcours en largeur) 
avec aff : 
pour affichage ou non du graphe avec le numero des sommets 


*****

Ne pas oublier de nettoyer avec make clean 
