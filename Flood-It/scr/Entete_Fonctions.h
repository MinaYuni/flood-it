#ifndef __ENTETE_FONCTIONS__
#define __ENTETE_FONCTIONS__

#include "API_Grille.h"
#include "API_Gene_instance.h"
#include "Liste_case.h"


/* 
 * Permet d’affecter dans la liste L les cases de la zone de la grille contenant la case (i, j)
 *
 * Entree : 
 * M : matrice entiere carree de dimensions dim*dim DEJA ALLOUEE
 * dim : dimension de la matrice
 * i,j : coordonées de la case 
 * taille : taille de la liste, soit nombre de cases de la zone 
 * L : liste des cases de la zone 
 */
void trouve_zone_rec(int **M, int dim, int i, int j, int *taille, ListeCase *L);


/* 
 * Utilise la fonction trouve_zone_rec pour jouer avec une séquence de jeu alétatoire 
 * (tirage au sort une couleur différente de la couleur de la Zsg)
 * Cette fonction reaffiche la grille apres modification
 *
 * Entree : 
 * M : matrice entiere carree de dimensions dim*dim DEJA ALLOUEE
 * G : la grille de jeu
 * dim : dimension de la matrice
 * nbcl : nombre de couleurs
 * aff : valeur booleenne pour l'affichage ou non de la grille 
 * 
 * Sortie : 
 * Retourne le nombre de changements de couleurs nécessaires pour gagner
 */
int sequence_aleatoire_rec(int **M, Grille *G, int dim, int nbcl, int aff);



#endif 
