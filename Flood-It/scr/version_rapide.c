#include <stdio.h>
#include <stdlib.h> 

#include "s_zsg.h"


/* Initialise la structure */
int init_Zsg(int dimension, int nbcolor, S_Zsg *zsg) {
	zsg->dim = dimension; 
	zsg->nbcl = nbcolor;
	
	//creation et allocation de la liste des cases de la zone 
	ListeCase Ltmp = (ListeCase)malloc(sizeof(ListeCase)); 
	if (Ltmp == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
	init_liste(&Ltmp); 
	zsg->Lzsg = Ltmp; // affectation de la liste des cases de la zone dans Lzsg
	
	//creation et allocation de la liste des cases de la bordure 
	ListeCase *Btmp = (ListeCase*)calloc(nbcolor, sizeof(ListeCase));
	if (Btmp == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}

	
	init_liste(Btmp);
	zsg->B = Btmp; // affectation de la bordure dans B
	
	/* creation et allocation de la matrice d'appartenance */
	int **Atmp = (int**)malloc(sizeof(int*)*dimension);
	if (Atmp == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	for (int i=0 ; i<dimension ; i++) {
		Atmp[i]= (int*)malloc(sizeof(int)*dimension);
		if (Atmp[i] == NULL) {
			printf("Erreur d'allocation memoire \n"); 
			return 0; 
		}
	}
	
	/* mise a -2 par defaut (pas d'appartenance dans Lzsg ou dans la bordure) */
	for (int i=0 ; i<dimension ; i++) {
		for (int j=0 ; j<dimension ; j++) {
			Atmp[i][j] = -2;
		}
	}
	
	zsg->App = Atmp; // affectation d'App 
}


/* Ajoute une case dans la liste Lzsg */
int ajoute_Zsg(int i, int j, S_Zsg *zsg) {
	int check; // pour verifier si l'ajout a bien ete fait 
	
	check = ajoute_en_tete(&(zsg->Lzsg), i, j);
	zsg->App[i][j] = -1; 
	
	return check; 
}


/* Ajoute une case de couleur cl dans la bordure */
int ajoute_Bordure(int i, int j, int cl, S_Zsg *zsg) { 
	int check; // pour verifier si l'ajout a bien ete fait 
	
	check = ajoute_en_tete(&(zsg->B[cl]), i, j);
	zsg->App[i][j] = cl; 
	
	return check; 
}


/* Renvoie 1 si une case est dans LZsg */
int appartient_Zsg(int i, int j, S_Zsg *zsg) {
	ListeCase liste = zsg->Lzsg;
	int **Atmp = zsg->App; 
	
	if(Atmp[i][j] == -1) {
		return 1; 
	}
    
	return 0;
}


/* Renvoie 1 si une case est dans la bordure de couleur cl donnee */
int appartient_Bordure(int i, int j, int cl, S_Zsg *zsg) {
	ListeCase *liste = zsg->B;
	int **Atmp = zsg->App; 
	
	if(Atmp[i][j] == cl) {
		return 1; 
	}
    
	return 0;
}


/* Met a jour les champs Lzsg et B d’une S_Zsg lorsque qu’une case (k,l) de couleur cl, 
qui est dans la bordure B[cl], doit basculer dans Lzsg (qui sera coloriee de couleur cl) */
int agrandit_Zsg(int **M, S_Zsg *Z, int cl, int k, int l) {
	int cpt = 0; // nombre de cases ajoutees à Lzsg 
	
	ListeCase *liste = (ListeCase*)malloc(sizeof(ListeCase)); // liste de cases a verifier
	if (liste == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	init_liste(liste); 

	if (!appartient_Zsg(k, l, Z) && M[k][l] == cl) { 
		ajoute_en_tete(liste, k, l);
		ajoute_Zsg(k, l, Z); 
		cpt++;
	}
	
	
	if (test_liste_vide(liste) == 0) {
		while(*liste) {
			int i, j; // coordonnees de la case a regarder
			enleve_en_tete(liste, &i, &j);
			
			/* On regarde la case a droite */
			if (i+1 < Z->dim) { // verifie qu'on ne sort pas de la grille
				if (!appartient_Zsg(i+1, j, Z) && !appartient_Bordure(i+1, j, M[i+1][j], Z)) {
					if (M[i+1][j] == cl) {
						ajoute_en_tete(liste, i+1, j);
						ajoute_Zsg(i+1, j, Z);
						cpt++;
					}
					else {
						ajoute_Bordure(i+1, j, M[i+1][j], Z);
					}
				}
			}

			/* On regarde la case a gauche */ 
			if (i-1 >= 0) {
				if (!appartient_Zsg(i-1, j, Z) && !appartient_Bordure(i-1, j, M[i-1][j], Z)) {
					if (M[i-1][j] == cl) {
						ajoute_en_tete(liste, i-1, j);
						ajoute_Zsg(i-1, j, Z);
						cpt++;
					}
					else {
						ajoute_Bordure(i-1, j, M[i-1][j], Z);
					}
				}
			}

			/* On regarde la case en bas */
			if (j+1 < Z->dim) {
				if (!appartient_Zsg(i, j+1, Z) && !appartient_Bordure(i, j+1, M[i][j+1], Z)) {
					if (M[i][j+1] == cl) {
						ajoute_en_tete(liste, i, j+1);
						ajoute_Zsg(i, j+1, Z);
						cpt++;
					}
					else {
						ajoute_Bordure(i, j+1, M[i][j+1], Z);
					}
				}
			}

			/* On regarde la case en haut */
			if (j-1 >= 0) {
				if (!appartient_Zsg(i, j-1, Z) && !appartient_Bordure(i, j-1, M[i][j-1], Z)) {
					if (M[i][j-1] == cl) {
						ajoute_en_tete(liste, i, j-1);
						ajoute_Zsg(i, j-1, Z);
						cpt++;
					}
					else {
						ajoute_Bordure(i, j-1, M[i][j-1], Z);
					}
				}
			}
		}
	}
	
	return cpt;
}


/* Algorithme tirant au sort une couleur en utilisant la fonction agrandit_Zsg pour determiner la Zsg, reaffichage de la grille apres modification */
int sequence_aleatoire_rapide(int **M, Grille *G, int dim, int nbcl, int aff) {
	S_Zsg *Z = (S_Zsg*)malloc(sizeof(S_Zsg*)); // création et allocation de la zone 
	if (Z == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	init_Zsg(dim, nbcl, Z); 
	
	int color = M[0][0]; // sauvegarde de la couleur de la zone 
	int nb_cases = agrandit_Zsg(M, Z, color, 0, 0); // nombre de cases dans la zone 
	
	int cpt = 0; // compteur pour le nombre de changement de couleurs 
	
    while (nb_cases != (dim*dim)) {
    	color = M[0][0];
        int tirage_couleur;
        do {
            tirage_couleur = rand()%nbcl; // tirage aleatoire de couleur parmi celles qu'on a
        } while (tirage_couleur == color); // tirage d'une couleur differente de la zone
		
        if (!test_liste_vide(&(Z->Lzsg))) { 
        	ListeCase lzsg_courante = Z->Lzsg;
            while (lzsg_courante) {
                M[lzsg_courante->i][lzsg_courante->j] = tirage_couleur; 
                
                if(aff == 1){
                    Grille_attribue_couleur_case(G, lzsg_courante->i, lzsg_courante->j, tirage_couleur);
                }
                
                lzsg_courante = lzsg_courante->suiv;
            }
        }
        
        if (!test_liste_vide(&(Z->B[tirage_couleur]))) {
        	while (Z->B[tirage_couleur]) {
        		nb_cases += agrandit_Zsg(M, Z, tirage_couleur, Z->B[tirage_couleur]->i, Z->B[tirage_couleur]->j); 
        		Z->B[tirage_couleur] = Z->B[tirage_couleur]->suiv;
        	}
        	
        	detruit_liste(&(Z->B[tirage_couleur]));
        }
        
        if(aff == 1) { // affichage de la grille
            Grille_redessine_Grille(G);
            Grille_attente_touche();
        }
		
        cpt++;
    }

	detruit_liste(&(Z->Lzsg));
	//free(Z);
	
    return cpt;   
}


/* Affiche une ListeCase */
int afficherListe(ListeCase liste) {
	if (liste == NULL) {
    	printf("Liste vide \n");
        return 0;
    }
    
	ListeCase tmp = liste; 
	
    while (tmp != NULL) {
        printf("i=%d \t j=%d \n", tmp->i, tmp->j);
        tmp = tmp->suiv;
    }
    
    return 1;
}

