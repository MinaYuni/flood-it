#ifndef __S_ZSG__
#define __S_ZSG__

#include "Liste_case.h"
#include "API_Grille.h"
#include "API_Gene_instance.h"


/* Structure de la Zone Superieure Gauche */
typedef struct {
	int dim;		/* dimension de la grille */
	int nbcl;		/* nombre de couleurs */
	ListeCase Lzsg;	/* Liste des cases de la zone Zsg */
	ListeCase *B;	/* Tableau de listes de cases de la bordure*/
	int **App;		/* Tableau a double entree des appartenances */
} S_Zsg;


/* 
 * Initialise la structure S_Zsg
 *
 * Entrees : 
 * dimension : dimension de la grille
 * nbcolor : nombre de couleurs 
 * zsg : structure à initialiser DEJA ALLOUEE
 * 
 * Sortie : 
 * Retourne 1 si l'initialisation a bien ete realisee, 0 sinon
 */
int init_Zsg(int dimemsion, int nbcolor, S_Zsg *zsg);


/* 
 * Ajoute une case (i,j) dans la liste Lzsg 
 * 
 * Entrees : 
 * i, j : coordonees de la case 
 * zsg : structure contenant la liste des cases de la zone zsg
 * 
 * Sortie : 
 * Retourne 1 si la case a bien ete ajoute, 0 sinon
 */
int ajoute_Zsg(int i, int j, S_Zsg *zsg);


/* 
 * Ajoute une case (i,j) de couleur cl dans la bordure 
 *
 * Entrees : 
 * i, j : coordonees de la case 
 * cl : couleur de la case
 * zsg : structure contenant la liste des cases de la bordure
 *
 * Sortie : 
 * Retourne 1 si la case a bien ete ajoute, 0 sinon 
 */
int ajoute_Bordure(int i, int j, int cl, S_Zsg *zsg);


/* 
 * Permet de savoir si une case (i,j) appartient a la liste des cases de la zone zsg
 * 
 * Entrees : 
 * i, j : coordonees de la case 
 * zsg : structure contenant la liste des cases de la bordure
 *
 * Sortie : 
 * Retourne 1 si la case appartient a la liste, 0 sinon
 */
int appartient_Zsg(int i, int j, S_Zsg *zsg);


/* 
 * Permet de savoir si une case (i,j) de couleur cl appartient a la bordure 
 * 
 * Entrees : 
 * i, j : coordonees de la case 
 * cl : couleur de la case
 * zsg : structure contenant la liste des cases de la bordure
 *
 * Sortie : 
 * Retourne 1 si la case appartient a la bordure, 0 sinon
 */
int appartient_Bordure(int i, int j, int cl, S_Zsg *zsg);


/* 
 * Met a jour les champs Lzsg et B d’une S_Zsg lorsque qu’une case (k,l) de couleur cl, 
 * qui est dans la bordure B[cl], doit basculer dans Lzsg (qui sera coloriee de couleur cl) 
 *
 * Entrees : 
 * M : matrice entiere carree DEJA ALLOUEE
 * Z : structure de la zone superieure gauche
 * cl : couleur de la case
 * k,l : coordonees de la case 
 *
 * Sortie : 
 * Retourne le nombre de cases qui a ete ajoutees à Lzsg 
 */
int agrandit_Zsg(int **M, S_Zsg *Z, int cl, int k, int l);


/*
 * Algorithme tirant au sort une couleur en utilisant la fonction agrandit_Zsg pour determiner la Zsg
 * Cette fonction reaffiche la grille apres modification
 * 
 * Entree : 
 * M : matrice entiere carree de dimensions dim*dim DEJA ALLOUEE
 * G : grille du jeu 
 * dim : dimension de la matrice 
 * nbcl : nombre de couleurs du jeu 
 * aff : valeur booleenne pour l'affichage ou non de la grille 
 *
 * Sortie : 
 * Retourne le nombre de changements de couleurs nécessaires pour gagner
 */
int sequence_aleatoire_rapide(int **M, Grille *G, int dim, int nbcl, int aff);


/*
 * Affiche une liste ListeCase
 * 
 * Entree : 
 * liste : liste de type ListeCase
 *
 * Sortie : 
 * Retourne 0 si la liste est vide, 1 sinon
 */
int afficherListe(ListeCase liste);


#endif
