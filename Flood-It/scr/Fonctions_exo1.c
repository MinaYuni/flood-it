#include <stdio.h>
#include <math.h>

#include "Entete_Fonctions.h"


/* Permet d’affecter dans la liste L les cases de la zone de la grille contenant la case (i, j) */
void trouve_zone_rec(int **M, int dim, int i, int j, int *taille, ListeCase *L){
	int couleur = M[i][j]; // sauvegarde des couleurs
	ajoute_en_tete(L, i, j); // ajout à la Liste L
  
	M[i][j] = -1;
	*taille = *taille + 1; // augmentation de la taille par rapport à l'ajout dans la liste 
	
	/* a droite */
	if ((i != dim-1) && (M[i+1][j] == couleur)) {
		trouve_zone_rec(M, dim, i+1, j, taille, L);
	}

	/* a gauche */
	if ((i != 0) && (M[i-1][j] == couleur)) {
		trouve_zone_rec(M, dim, i-1, j, taille, L);
	}
	
	/* en bas */
	if ((j != dim-1) && (M[i][j+1] == couleur)) {
		trouve_zone_rec(M, dim, i, j+1, taille, L);
	}

	/* en haut */
	if ((j != 0) && (M[i][j-1] == couleur)) {
		trouve_zone_rec(M, dim, i, j-1, taille, L);
	}
}


/* Utilise la fonction trouve_zone_rec pour jouer avec une séquence de jeu alétatoire (tirage au sort une couleur différente de la couleur de la Zsg) avec reaffichage de la grille apres modification */
int sequence_aleatoire_rec(int **M, Grille *G, int dim, int nbcl, int aff){
	/* creation et allocation de la liste des cases de la zone */
	ListeCase *Li = (ListeCase *)malloc(sizeof(ListeCase)); 
	init_liste(Li);

	int color = M[0][0]; // sauvegarde de la couleur de la zone 

	int taille = 0; 
	int *taille_zone = &taille;

	/* mise a jour de la liste et de la taille de la zone */
	trouve_zone_rec(M, dim, 0, 0, taille_zone, Li); 

	if (*taille_zone == (dim*dim)) { // verification de la fin du jeu
		detruit_liste(Li);
		return 1;
	}
	else {
		int tirage_couleur;
		do {
			tirage_couleur = rand()%nbcl; // tirage aleatoire de couleur parmi celles qu'on a
		} while (tirage_couleur == color); // tirage d'une couleur differente de la zone

		if(test_liste_vide(Li) == 0) { 
			while(*Li) {
				M[(*Li)->i][(*Li)->j] = tirage_couleur; // remplissage des cases de la zone par la couleur tiree
				if(aff == 1){
					Grille_attribue_couleur_case(G, (*Li)->i, (*Li)->j, tirage_couleur);
				}
				
				(*Li) = (*Li)->suiv;
			}
		}

		if(aff == 1) { // affichage de la grille
			Grille_redessine_Grille(G);
			Grille_attente_touche();
		}

		detruit_liste(Li);
		
		return (1 + sequence_aleatoire_rec(M, G, dim, nbcl, aff)); 
	}

}


