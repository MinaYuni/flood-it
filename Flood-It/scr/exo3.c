#include <stdio.h>
#include <stdlib.h> 

#include "s_zsg.h"
#include "Liste_case.h"


int main() {
	int dimension = 5; 
	int nbcolor = 6; 
	int i1 = 0, j1 = 1; 
	int i2 = 2, j2 = 3;
	int cl = 4; 
	
	S_Zsg *zsg = (S_Zsg*)malloc(sizeof(S_Zsg));
	if (zsg == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}

	init_Zsg(dimension, nbcolor, zsg); 

	ajoute_Zsg(i1, j1, zsg);
	ajoute_Bordure(i2, j2, cl, zsg);
	
	printf("Lzsg : \n"); 
	afficherListe(zsg->Lzsg);

	printf("Bordure : \n"); 
	afficherListe(zsg->B[cl]);
	
	detruit_liste(&(zsg->Lzsg));
	detruit_liste(zsg->B);
	free(zsg);
}
