#include <stdio.h>
#include <stdlib.h> 

#include "graphe_zone.h"
#include "s_zsg.h"


/* Ajoute un pointeur sur Sommet a une liste chainee de Cellule_som passee en parametre */
int ajoute_liste_sommet(Cellule_som **cell_som, Sommet *s) {
	Cellule_som *nouveau = (Cellule_som*)malloc(sizeof(Cellule_som));
	if (nouveau == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
	nouveau->suiv = (*cell_som);
	nouveau->sommet = s;
	(*cell_som) = nouveau; 
	
	return 1; 
}


/* Detruit une liste chainee de Cellule som SANS detruire les sommets pointees par cette liste */
void detruit_liste_sommet(Cellule_som *cell_som) {
	free(cell_som);
}


/* Met a jour deux sommet s1 et s2 en indiquant qu’ils sont adjacents l’un de l’autre */
int ajoute_voisin(Sommet *s1, Sommet *s2) {
	int check1, check2; 
	
	Sommet *cp1 = (Sommet*)malloc(sizeof(Sommet));
	Sommet *cp2 = (Sommet*)malloc(sizeof(Sommet));
	if (cp1 == NULL || cp2 == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
	cp1 = s1;
	cp2 = s2;

	check1 = ajoute_liste_sommet(&(cp1)->sommet_adj, cp2);
	check2 = ajoute_liste_sommet(&(cp2)->sommet_adj, cp1);
	
	return (check1 && check2);
}


/* Renvoie 1 si et seulement si deux sommets sont adjacents */
int adjacent(Sommet *s1, Sommet *s2) {
	Cellule_som *tmp1 = (Cellule_som*)malloc(sizeof(Cellule_som));
	Cellule_som *tmp2 = (Cellule_som*)malloc(sizeof(Cellule_som));
	
	if (tmp1 == NULL || tmp2 == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
	tmp1 = s1->sommet_adj;
	tmp2 = s2->sommet_adj;

	while (tmp2) {
		if (tmp2->sommet == s1) {
			return 1;
		}
		tmp2 = tmp2->suiv;
	}
	
	return 0;
}


/* Determine la zone correspondante a la case (x,y) de la grille quand graphe.mat[x][y]==NULL */
int trouve_zone(int **M, int dim, Graphe_zone *graphe, int x, int y) {	
	ListeCase *liste_courante = (ListeCase*)malloc(sizeof(ListeCase)); // liste des cases a verifier
	Sommet *sommet = (Sommet*)malloc(sizeof(Sommet)); // sauvegarde du sommet pointe par la matrice[x][y]
	Sommet *s = (Sommet*)malloc(sizeof(Sommet)); // sommet correspondante a la case (x,y)
	ListeCase liste_zone = (ListeCase)malloc(sizeof(ListeCase)); // liste pour initialiser le sommet s 
	
	if (liste_courante == NULL || sommet == NULL || s  == NULL || liste_zone == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
	init_liste(liste_courante);
	
	/* Initialisation du sommet */
	s->pere = NULL;
	s->distance = -1;
	s->num = graphe->nbsom; 
	s->cl = M[x][y]; 
	s->cases = liste_zone; 
	s->nbcase_som = 0;
	s->marque = 2;
	
	ajoute_en_tete(&(s->cases), x, y); // ajout de la case(x,y) dans le sommet
	ajoute_en_tete(liste_courante, x, y); 
	
	graphe->mat[x][y] = s;

	sommet = graphe->mat[x][y];
	
	int color = sommet->cl; // sauvegarde de la couleur du sommet 
	
	/* On determine la zone */
	if (test_liste_vide(liste_courante) == 0) {
		while(*liste_courante) {
			int i, j; // coordonnees de la case a regarder
			enleve_en_tete(liste_courante, &i, &j);

			/* On regarde la case a droite */
			if (i+1 < dim) { // verifie qu'on ne sort pas de la grille
				if (graphe->mat[i+1][j] == NULL) {
					if (M[i+1][j] == color) {
						ajoute_en_tete(liste_courante, i+1, j);
						ajoute_en_tete(&(sommet->cases), i+1, j);
						graphe->mat[i+1][j] = graphe->mat[x][y];
						sommet->nbcase_som++;
					}
				}
			}
			
			/* On regarde la case a gauche */
			if (i-1 >= 0) { 
				if (graphe->mat[i-1][j] == NULL) {
					if (M[i-1][j] == color) {
						ajoute_en_tete(liste_courante, i-1, j);
						ajoute_en_tete(&(sommet->cases), i-1, j);
						graphe->mat[i-1][j] = graphe->mat[x][y];
						sommet->nbcase_som++;
					}
				}
			}
			
			/* On regarde la case en bas */
			if (j+1 < dim) { 
				if (graphe->mat[i][j+1] == NULL) {
					if (M[i][j+1] == color) {
						ajoute_en_tete(liste_courante, i, j+1);
						ajoute_en_tete(&(sommet->cases), i, j+1);
						graphe->mat[i][j+1] = graphe->mat[x][y];
						sommet->nbcase_som++;
					}
				}
			}
			
			/* On regarde la case en haut */
			if (j-1 >= 0) {
				if (graphe->mat[i][j-1] == NULL) {
					if (M[i][j-1] == color) {
						ajoute_en_tete(liste_courante, i, j-1);
						ajoute_en_tete(&(sommet->cases), i, j-1);
						graphe->mat[i][j-1] = graphe->mat[x][y];
						sommet->nbcase_som++;
					}
				}
			}
		}
	}
	
	return 1; 
}


/* Cree le graphe entier */
int cree_graphe_zone(int **M, Graphe_zone *graphe, int dim, int nbcl) {
	Cellule_som *cell_som = (Cellule_som*)malloc(sizeof(Cellule_som)); // liste des sommets du graphe 
	Sommet ***mat = (Sommet***)malloc(sizeof(Sommet**)*dim); // matrice de pointeurs sur les ommets indiquant a quel sommet appartient une case de la grille 
	
	if (cell_som == NULL || mat == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
	/* Initialisation de la matrice mat qui contient le pointeur NULL pour chaque case */
	for (int i=0 ; i<dim ; i++) {
		mat[i] = (Sommet**)malloc(sizeof(Sommet*)); 
		if (mat[i]  == NULL) {
			printf("Erreur d'allocation memoire \n"); 
			return 0; 
		}
		for (int j=0 ; j<dim ; j++) {
			mat[i][j] = (Sommet*)malloc(sizeof(Sommet)); 
			if (mat[i][j]  == NULL) {
				printf("Erreur d'allocation memoire \n"); 
				return 0; 
			}			
			mat[i][j] = NULL;			
		}
	}
	
	/* Initialisation de la structure graphe avec 0 sommet */
	graphe->mat = mat;
	graphe->nbsom = 0; 
	graphe->som = cell_som; 
	
	/* pour chaque case (i,j) de la grille, creer un sommet vide de cases */
	for (int i=0 ; i<dim ; i++) {
		for (int j=0 ; j<dim ; j++) {
			if (graphe->mat[i][j] == NULL) {
				trouve_zone(M, dim, graphe, i, j);
				ajoute_liste_sommet(&(graphe->som), graphe->mat[i][j]);
				graphe->nbsom++;
			}
		}
	}
	
	/* Indique quelles sont les sommets voisins en parcourant a nouveau toutes les cases de la grille */ 
	// parcours en ligne 
	for (int i=0 ; i+1<dim ; i++) {
		for (int j=0 ; j<dim ; j++) {
			if (graphe->mat[i][j] != graphe->mat[i+1][j]) {
				if(!adjacent(graphe->mat[i][j], graphe->mat[i+1][j])) {
					ajoute_voisin(graphe->mat[i][j], graphe->mat[i+1][j]);
				}
			}
		}
	}
	//parcours en colonne
	for (int i=0; i<dim ; i++) {
		for (int j=0; j+1<dim; j++) {
			if (graphe->mat[i][j] != graphe->mat[i][j+1]) {
				if(!adjacent(graphe->mat[i][j], graphe->mat[i][j+1])) {
					ajoute_voisin(graphe->mat[i][j], graphe->mat[i][j+1]);
				}
			}
		}
	}
	
	return 1;
}


/* Affiche le graphe par numero de sommet */
void affiche_graphe(Graphe_zone *graphe, int dim) {
	for (int i=0 ; i<dim ; i++) {
		for (int j=0 ; j<dim ; j++) {
			printf("%d\t", graphe->mat[i][j]->num);
		}
		printf("\n");
	}
	
}


/* Affiche le numéro de chaque sommet de la liste chainee Cellule_som */
int affiche_cellule_som(Cellule_som *cell_som) {
	if (cell_som == NULL) {
		printf("Liste Vide \n");
		return 0;
	}
	
	Cellule_som *tmp = cell_som; 
	
    while (tmp != NULL) {
        printf("num_sommet=%d\tcouleur: %d\tmarque: %d\n", tmp->sommet->num, tmp->sommet->cl, tmp->sommet->marque);
        tmp = tmp->suiv;
    }
	
	return 1;
}


/* Met a jour le graphe dont la zsg et sa bordure */
int bordure_graphe(Graphe_zone *graphe, Cellule_som **zsg ,int cl) {
	Cellule_som *tmp = graphe->som; // liste des sommets du graphe 
	Cellule_som *zsg_tmp = (Cellule_som*)malloc(sizeof(Cellule_som)); // liste des sommets de la zsg
	if (zsg_tmp == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
	/* Boucle for ajoutant les sommets qui sont adjacents à la zsg et de meme couleur que la zsg
	dans la liste des sommets étant marqué comme appartenant à la zone zsg */
	for(int i = 0 ; i < graphe->nbsom ; i++, tmp = (tmp)->suiv) {
		if (tmp->sommet->marque == 1) {
			if(tmp->sommet->cl == cl) {
				tmp->sommet->marque = 0;
				ajoute_liste_sommet(zsg, tmp->sommet);
			}
		}			
	}
	
	zsg_tmp = *zsg;
	
	/* Met a jour la bordure en marquant les sommets avec 1 */
	while(zsg_tmp) {
		Cellule_som *zsg_adj = (zsg_tmp)->sommet->sommet_adj;
			while(zsg_adj != NULL) {
				if(zsg_adj->sommet->marque == 2) {
					zsg_adj->sommet->marque = 1;
				}
				zsg_adj = zsg_adj->suiv;
			}		
		zsg_tmp = zsg_tmp->suiv;
	}
	
	return 1;
}


/* 
 * Regarde si le jeu est terminee 
 * Le jeu s’arrête quand la bordure est vide pour toute couleur 
 */
int check_fin(Graphe_zone *graphe) {
	int n=0;
	Cellule_som *tmp = graphe->som;
	
	for(int i=0 ; i < graphe->nbsom ; i++, tmp = (tmp)->suiv) {
		if (tmp->sommet->marque > 0) {
			n++;
			return 0;
		}			
	}
	
	return 1;
}


/* Renvoie l'indice de la valeur maximale du tableau */
int max_tableau(int *tab, int taille){
	int indice_max = 0;
	int max = tab[0];
	
	for(int i=0 ; i < taille ; i++) {
		if(tab[i] > max) {
			max = tab[i];
			indice_max = i;
		}
	}
	
	return indice_max;
}


/* Renvoie la couleur de la bordure qui a le plus de sommets */
int max_bordure(Graphe_zone *graphe, int nbcl) {
	int *tab_couleur = (int*)calloc(nbcl, sizeof(int)); // tableau par couleur du nombre de sommets de cette couleur 
	if (tab_couleur == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
	/* Initialisation du tableu de couleur */
	for (int i=0 ; i < nbcl ; i++) {
		tab_couleur[i] = 0;
	}
	
	Cellule_som *tmp = graphe->som; // liste des sommets du graphe
	
	for(int i=0 ; i < graphe->nbsom ; i++, tmp = (tmp)->suiv) {
		if (tmp->sommet->marque == 1) {
			tab_couleur[tmp->sommet->cl]++;
		}			
	}
	
	return max_tableau(tab_couleur, nbcl);
}


/* Resolution du jeu */
int jeu(Graphe_zone *graphe, Cellule_som **zsg,int nbcl, int *BFS, int taille_bfs) {
    int nb_coup = 0;
    int compteur_bfs = 0;
    while(!check_fin(graphe)) {
        int couleur_choisie;
        if(compteur_bfs < taille_bfs) {
            couleur_choisie = BFS[compteur_bfs++];
        }
        else {
            couleur_choisie = max_bordure(graphe, nbcl);
        }
        bordure_graphe(graphe, zsg, couleur_choisie);
        nb_coup++;        
    }
    
    return nb_coup;
}


/* Parcours en largeur du graphe jusqu'au sommet cible */
int BFS(Graphe_zone *graphe, Sommet *depart, Sommet *cible) {
    for(int profondeur=0 ; profondeur<100 ; profondeur++) {
        Cellule_som *pere_tmp = graphe->som; // sauvegarde de la liste des sommets du graphe
        for(int i=0; i < graphe->nbsom ; i++, pere_tmp = (pere_tmp)->suiv) {
            if (pere_tmp->sommet->distance == profondeur) { // on verifie de la bonne profondeur du parcours en largeur 
                Cellule_som *sommet_adj = (pere_tmp)->sommet->sommet_adj; // sauvegarde de tous les adjacents du sommet en cours de parcours 
                while(sommet_adj != NULL) { // parcours des adjacents 
                    if(sommet_adj->sommet->distance == -1) { // on verifie que le sommet n'a pas encore ete visite (initialise a -1) 
                        sommet_adj->sommet->distance = profondeur+1; // on affecte la profondeur du sommet nouvellement visite a pronfondeur+1 (car fils) 
                        sommet_adj->sommet->pere = pere_tmp->sommet; // on affecte le pere du sommet qu'on vient de visiter 
                    }
                    if(sommet_adj->sommet == cible) { // on termine la recherche si on trouve la cible 
                    	return (sommet_adj->sommet->distance); // retourne la distance a laquelle on a trouve la cible 
                   	}
                    sommet_adj = sommet_adj->suiv;
                }
            } 
        }
    }
    
    return 0;
}


/* Cherche le chermin inverse a parcourir a partir du sommet vise jusqu'au sommet de base qui est le sommet qui appartient a la zsg au debut du jeu */
int *chemin_inverse(Sommet *vise) {
    int* chemin = (int*)calloc(vise->distance, sizeof(int));
    if (chemin == NULL) {
		printf("Erreur d'allocation memoire \n"); 
		return 0; 
	}
	
    Sommet *sommet_pere = vise->pere; // sauvegarde du sommet vise 
    
    while(sommet_pere != NULL) { // on cherche le pere originel en remontant l'arbre genealogique 
        if (sommet_pere->distance > 0) { 
            chemin[sommet_pere->distance-1] = sommet_pere->cl; // sauvegarde des couleurs necessaires pour faire ce chemin 
        }
        sommet_pere = sommet_pere->pere;
    }
    
    return chemin;
}
